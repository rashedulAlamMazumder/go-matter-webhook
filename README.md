# gowebhook


## Installation
```sh
$ go get -u gitlab.com/rashedulAlamMazumder/go-matter-webhook/
```

## Usage

Basic text message

```go
   webhookURL := "https://docs.mattermost.com/developer/webhooks-incoming.html"
   message := hook.Message{
      Text: "Hello MatterHook",
   }
   err := hook.Send(weebhokURL, message)
   if err != nil {
      fmt.Println(err)
   }

```

Send message with attachments
```go
    webhookURL := "https://matterhook.com/3423knkldv323"
  
    var msg matterhook.Message
    
    attachment := hook.Attachment{
        Text:  "Attact Text",
        Title: "Attact Title",
        Fields: []matterhook.Field{
            {
                Title: "Attach field Title",
            },
        },
    }
    
    anotherAttachment := hook.Attachment{
        Text:       "Hello",
        Title:      "No title",
        AuthorName: "nafis",
        AuthorIcon: "give author icon",
        Fields: []hook.Field{
            {
                Title: "Filed Title",
                Value: "Some value",
                Short: true,
            },
        },
    }
    
    msg.AddAttachments([]hook.Attachment{attachment, anotherAttachment})
    
    err := hook.Send(webhookURL, msg)
    if err != nil {
        fmt.Println(err)
    }
```

## Contribution
If you are interested to make the package better please send pull requests or create an issue so that others can fix.
